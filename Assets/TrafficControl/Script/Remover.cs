﻿using UnityEngine;
using System.Collections;

public class Remover : MonoBehaviour 
{
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Car") 
		{
			Car car = col.gameObject.GetComponent<Car>();
			car.isOut = true;
			GameManager.instance.removeCar(car);
			Destroy (col.gameObject, 3.0f);
		}
	}
}
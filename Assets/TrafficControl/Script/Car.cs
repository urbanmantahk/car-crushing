﻿using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour 
{
	enum CAR_STATE
	{
		STOP,
		GO,
		ACCELERATE
	}

	public GameObject m_light;
	public ParticleSystem m_particle;

    public bool isHuman = false;

	private float dragSensitive = 1.0f;
	private float m_speed = 8.0f;
	private float m_accSpeed = 16.0f;
	private string m_directionTag = "";
	private Transform m_frontCheck;
	private bool m_tempStop = false;
	private bool m_isOut = false;
	private CAR_STATE m_state = CAR_STATE.GO;

	void Start () 
	{
		GameManager.instance.AddCar (this);
		m_frontCheck = transform.Find("front").transform;
		m_particle.Stop ();

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
        CarControl();

		if (m_tempStop == false) 
		{
			if(checkFront ())
				StartCoroutine(tempStop());
		}
	}

	public float speed
	{
		set
		{
			m_speed = value;
		}
	}

	public float accSpeed
	{
		set
		{
			m_accSpeed = value;
		}
	}

	public bool isOut
	{
		set
		{
			m_isOut = value;
		}
		get
		{
			return m_isOut;
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (isOut == true)
			return;

		Car car = coll.gameObject.GetComponent<Car>();
		if (car == null)
			return;

        if (!car.isHuman)
            return;

		if (car.directionTag != m_directionTag) 
		{
			accident (coll);
		}
		else
		{
			if(m_state == CAR_STATE.ACCELERATE)
			{
				accident (coll);
			}
		}
	}

	public string directionTag
	{
		get { return m_directionTag; }
		set { m_directionTag = value; }
	}
	
	public void go()
	{
		if (m_state == CAR_STATE.STOP)
			return;

        if (isHuman)
            return;

		m_state = CAR_STATE.GO;

		Vector3 forward = transform.up;
		forward *= m_speed;
		GetComponent<Rigidbody2D>().velocity = forward;
	}

	public void accelerate()
	{
		if (m_state == CAR_STATE.STOP)
			return;

		m_particle.Play ();
		GetComponent<AudioSource>().Play ();
		m_state = CAR_STATE.ACCELERATE;

		Vector3 forward = transform.up;
		forward *= m_accSpeed;
		GetComponent<Rigidbody2D>().velocity = forward;
	}

	public void stop()
	{
		m_state = CAR_STATE.STOP;
		m_particle.Stop ();
		GetComponent<Rigidbody2D>().drag = 3.0f;
		StopAllCoroutines ();
	}
	
	private bool checkFront()
	{
		if (m_state == CAR_STATE.ACCELERATE 
			|| m_state == CAR_STATE.STOP )
		{
			return false;
		}

		Collider2D[] frontHits = Physics2D.OverlapPointAll(m_frontCheck.position, 1);

		foreach(Collider2D c in frontHits)
		{
			Car car = c.GetComponent<Car>();
			if(c.tag == "Car" 
			  && car != null
			  && car.directionTag == directionTag)
			{
				return true;
			}
		}

		return false;
	}

	private void accident(Collision2D coll)
	{
		GetComponent<Rigidbody2D>().drag = 5.0f;
		SpawnerManager.instance.stopAll ();

		GameManager.instance.stopAll ();
		GameManager.instance.crash(coll);
		GameManager.instance.makeDialog ();
	}

	private IEnumerator tempStop()
	{
		m_tempStop = true;
		GetComponent<Rigidbody2D>().drag = 5.0f;

		while (checkFront()) 
		{
			yield return new WaitForSeconds (1.5f);
		}
		m_tempStop = false;
		GetComponent<Rigidbody2D>().drag = 0.0f;
		go ();

		yield return null;
	}

	private IEnumerator touchStop()
	{
		yield return new WaitForSeconds (2.0f);

		for(int i=0; i<2; i++)
		{
			m_light.SetActive(true);
			yield return new WaitForSeconds (0.3f);
			m_light.SetActive(false);
			yield return new WaitForSeconds (0.3f);
		}

		go ();
		yield return null;
    }

    #region HUMAN_CONTROL
    private bool isTurning = false;
    private bool isForward = true;

    void CarControl()
    {
        if (!isHuman)
            return;

        float _speed = 3f;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            isForward = true;
            Vector3 forward = transform.up;
            forward *= _speed;
            GetComponent<Rigidbody2D>().velocity = forward;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            isForward = false;
            Vector3 back = transform.up;
            back *= _speed * -1f;
            GetComponent<Rigidbody2D>().velocity = back;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            isTurning = true;
            float _rotation = 100f;
            if (!isForward)
                _rotation *= -1f;

            StartCoroutine(TurnRoutine(_rotation));
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            isTurning = true;
            float _rotation = -100f;
            if (!isForward)
                _rotation *= -1f;

            StartCoroutine(TurnRoutine(_rotation));
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
            isTurning = false;
    }

    IEnumerator TurnRoutine(float _rotation)
    {
        while (isTurning)
        {
            _rotation *= Time.deltaTime;
            transform.Rotate(0, 0, _rotation);
            yield return new WaitForSeconds(0.1f);
        }
    }


    #endregion


}

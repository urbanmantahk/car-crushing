﻿using UnityEngine;
using System.Collections;

public class TouchEffect : MonoBehaviour {
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseUp()
	{
		StartCoroutine (shake());
	}

	private IEnumerator shake()
	{
		float scaleOff = 0.02f;
		// small
		for(int i=0; i<5; i++)
		{
			Vector3 scale = transform.localScale;
			transform.localScale = new Vector3(scale.x - scaleOff, scale.y - scaleOff, 1);
			yield return new WaitForFixedUpdate();
		}

		// big
		for(int i=0; i<5; i++)
		{
			Vector3 scale = transform.localScale;
			transform.localScale = new Vector3(scale.x + scaleOff, scale.y + scaleOff, 1);
			yield return new WaitForFixedUpdate();
		}

		yield return null;
	}


}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;	

public class SpawnerManager : MonoBehaviour 
{
	// static
	private static SpawnerManager  m_instance; 
	public static SpawnerManager instance { get { return m_instance; } }

	// speed
	public float m_carMinSpeed = 26.0f;
	public float m_carMaxSpeed = 60.0f;
	public float m_carAccSpeed = 36.0f;

	public float m_spawnMinDelay = 2.0f;
	public float m_spawnMaxDelay = 4.0f;

	public GameObject[] m_cars;
	public Spawner[] m_spawners;

	void Awake()
	{
		m_instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		StartCoroutine ("SpawnCars");
        StartCoroutine (SpeedUpSpawn());
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
	
	public void stopAll()
	{
		StopCoroutine ("SpawnCars");
	}

    IEnumerator SpeedUpSpawn()
    {
        float _time = Time.time;
        while (true)
        {
            yield return new WaitForSeconds(1f);
            _time = Time.time;

            if (m_spawnMinDelay > 1f)
                m_spawnMinDelay -= _time * 0.27f;

            if (m_spawnMaxDelay > 1f)
                m_spawnMaxDelay -= _time * 0.32f;
        }
    }

	IEnumerator SpawnCars()
	{
		while (true)
		{
			yield return new WaitForSeconds(Random.Range(m_spawnMinDelay, m_spawnMaxDelay));
			Spawner spawner = m_spawners[Random.Range(0, m_spawners.Length)];
			//Spawner spawner = m_spawners[0];

			while(checkFront(spawner) == true)
			{
				yield return new WaitForFixedUpdate();
				spawner = m_spawners[Random.Range(0, m_spawners.Length)];
				//spawner = m_spawners[0];
			}

			int index = Random.Range (0, m_cars.Length);
			GameObject gameObject =  Instantiate (m_cars [index], spawner.transform.position, spawner.transform.rotation) as GameObject;
			Car car = gameObject.GetComponent<Car>();
			car.directionTag = spawner.m_direction;
			car.speed = Random.Range(m_carMinSpeed, m_carMaxSpeed);
			car.accSpeed = m_carAccSpeed;
			car.go();
		}
		yield return null;
	}

	private bool checkFront(Spawner spawner)
	{
		Vector3 checkVector = spawner.transform.position + spawner.transform.up * 1.5f;
		Collider2D[] frontHits = Physics2D.OverlapCircleAll(checkVector, 2);

		foreach(Collider2D c in frontHits)
		{
			if(c.tag == "Car" )
			{
				return true;
			}
		}
		return false;
	}
	
	public void stop()
	{
		StopCoroutine ("SpawnCars");
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;	


public class GameManager : MonoBehaviour {
	private static GameManager  m_instance; 
	public static GameManager instance { get { return m_instance; } }

	public GameObject m_smoke;
	public GameObject m_dialog;
	public AudioClip m_audio;

    public Text timer_text;

    private bool gameOver = false;

    private float timeCount = 0f;

	private List<Car> m_cars = new List<Car> ();

	void Awake()
	{
		m_instance = this;
	}
	// Use this for initialization
	void Start () {
        timeCount = 0f;
        StartCoroutine(StartTimer());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator StartTimer()
    {
        while(!gameOver)
        {
            timeCount += Time.deltaTime;
            timer_text.text = string.Format("Time = {0}", timeCount.ToString("0.00"));
            yield return new WaitForEndOfFrame();
        }
    }
	
	public void AddCar(Car car)
	{
		m_cars.Add (car);
	}
	public void removeCar(Car car)
	{
		m_cars.Remove (car);
	}

	public void stopAll()
	{
        gameOver = true;

		foreach(Car car in m_cars)
		{
			car.stop();
		}
	}

	public void crash(Collision2D col)
	{
		// smoke
		Vector3 vec = col.transform.position;
		vec.z = -1.0f;
		Instantiate (m_smoke, vec, new Quaternion());

		// sound
		AudioSource.PlayClipAtPoint (m_audio, vec);
	}

	public void makeDialog()
	{
		StartCoroutine (makeDialogCoroutine ());
	}

	IEnumerator makeDialogCoroutine()
	{
		yield return new WaitForSeconds (3.0f);
		m_dialog.SetActive (true);

		yield return null;
	}
}
